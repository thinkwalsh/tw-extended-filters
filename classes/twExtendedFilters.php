<?php
/**
 *
 * Written By: Daniel Clements
 * Date: 
 * Copyright: Think Walsh Creative Solutions
 * Versione 0.1
 *
 */

class twExtendedFilters
{
    
    function twExtendedFilters()
    {
        //Constructor
    }
    

    public static function floatFilter( $params )
    {
        $table = null;
        $joins = null;
        $columns = null;
        $filter = $params['filter'];
        
        //set operator
        if ( isset($filter['operator']) )
            $operator = $filter['operator'];
        else
            $operator = 'and';
            
        //get filter params
        if ( isset($filter['params']) and is_array($filter['params']) )
        {
            $table = 'ezcontentobject_attribute';
            
            //join to the attribute table by content object ID and also by current version to make sure we get the objects with the current version.
            $joins .= ' ezcontentobject_tree.contentobject_id = '.$table.'.contentobject_id AND ezcontentobject_tree.contentobject_version = '.$table.'.version AND ';
            
            $params = $filter['params'];
            
            $attributeIDs = array();
            
            //check if we have multiple params to retreive
            if ( is_array($params[0]) )
            {
                foreach ($params as $index => $param)
                {
                    $attributeID = $param[0];
                    $searchOperator = $param[1];
                    $searchValue = $param[2];

                    if ( is_numeric($attributeID) === false )
                    {
                        $attributeID = eZContentClassAttribute::classAttributeIDByIdentifier( $attributeID );
                    }
                    
                    $attributeIDs[] = $attributeID;
                    $searchOperators[] = $searchOperator;
                    $searchValues[] = $searchValue;
                }
            }
            else
            {
                $attributeID = $params[0];
                $searchOperator = $params[1];
                $searchValue = $params[2];

                if ( is_numeric($attributeID) === false )
                {
                    $attributeID = eZContentClassAttribute::classAttributeIDByIdentifier( $attributeID );
                }
                
                $attributeIDs[] = $attributeID;
                $searchOperators[] = $searchOperator;
                $searchValues[] = $searchValue;
                
            }
            
            //build $joins starting with content class attribute id search
            $joins .= ' '.$table.'.contentclassattribute_id IN ('.implode(', ', $attributeIDs).') AND ';
            
            //then the search of the float fields
            $total = count($params);
            foreach ($searchOperators as $index => $searchOperator)
            {
                $joins .= ' '.$table.'.data_float'.$searchOperator.(double)$searchValues[$index];
                if ( $index == --$total )
                    $joins .= ' AND ';
                else
                    $joins .= ' '.strtoupper($operator).' ';
            }
        }
        
        return array(
            'tables'  => ','.$table,
            'joins'   => $joins,
            'columns' => $columns
        );
    }
    
    public static function multiCheckboxComparison( $params ) 
    {
        $classAttribute=array();
        $comparisonOperator=array();
        $value=array();
        
        if ( isset( $params ) and is_array($params) ) 
        {
            foreach ( $params['attributes'] as $filter )
            {
                if( is_numeric( $filter ) === false )
                    $classAttribute[] = eZContentClassAttribute::classAttributeIDByIdentifier( $filter );
                else
                    $classAttribute[] = $filter;                  
            }
        }
        else
            return array();
            
        $value = (int)$params['status'];

		$db           = eZDB::instance();
		$attributeVar = 'fc' . $params['index'];
		$tables       = ', ezcontentobject_attribute as ' . $attributeVar;
		$joins        = $attributeVar . '.contentobject_id = ezcontentobject.id AND ' .
			$attributeVar . '.version = ezcontentobject.current_version AND ';
        
        $joins .= $attributeVar . '.contentclassattribute_id IN ( '.implode(', ', $classAttribute). ' ) AND '
               . $attributeVar . '.data_int = ' . $db->escapeString( $value ).' AND ';
        
		return array(
			'tables'  => $tables,
			'joins'   => $joins
		);
	}
}

?>